import logging
import os
import subprocess
import time

import utils

class TestApplication:
    GlobTimeLimit = 10

    @classmethod
    def Create(cls, fpath, ctx):
        if fpath.lower() == "makefile":
            return MakefileApplication(fpath)
        fname, fext = os.path.splitext(fpath)
        app_cls = {".py": PyApplication, ".cpp": CppApplication, "": BinApplication}[fext]
        return app_cls(fpath, ctx)

    def __init__(self, fpath, ctx):
        self.SourceFile = os.path.abspath(fpath)
        self.WorkingDirectory = ctx.WorkingDirectory
        self.Compile()

    def Run(self, workingDir, limitTm=GlobTimeLimit):
        proc = subprocess.Popen([self.ApplicationFile], cwd=workingDir)
        stTime = time.time()
        while time.time() < stTime + limitTm:
            if proc.poll() is not None:
                return proc.poll(), time.time() - stTime
            time.sleep(0.01)
        exitCode = proc.poll()
        if exitCode is None:
            proc.kill()
            raise utils.TimeLimitError(time.time() - stTime, limitTm)
        return exitCode, time.time() - stTime

    def RunCompilation(self, shell_args):
        logging.info("compilation shell_args: \"%s\"", shell_args)
        exitCode, outData, errData = utils.run_app(shell_args, cwd=self.WorkingDirectory)
        if exitCode != 0:
            raise utils.CompileError(exitCode, outData, errData)


class CppApplication(TestApplication):

    def Compile(self):
        fname = os.path.splitext(os.path.basename(self.SourceFile))[0]
        self.ApplicationFile = os.path.abspath((os.path.join(self.WorkingDirectory, fname)))
        self.RunCompilation(["g++", self.SourceFile, "-std=c++0x", "-O3", "-o", fname])


class PyApplication(TestApplication):
    def Compile(self):
        self.ApplicationFile = os.path.abspath(self.SourceFile)
        if not os.access(self.ApplicationFile, os.X_OK):
            raise utils.CompileError(0, "", "{0} is not executable file".format(self.ApplicationFile))
    

class BinApplication(TestApplication):
    def Compile(self):
        self.ApplicationFile = os.path.abspath(self.SourceFile)
        if not os.access(self.ApplicationFile, os.X_OK):
            raise utils.CompileError(0, "", "{0} is not executable file".format(self.ApplicationFile))


class MakefileApplication(TestApplication):
    def Compile(self):
        self.RunCompilation(["make", "-C", os.path.dirname(self.SourceFile)])
        #TODO: fetch filename from makefile
