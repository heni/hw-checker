#!/usr/bin/env python2.7
# workaround: 
#    ./run_tests.py --file test.cpp --checker tests_01
#    ./run-tests.py --file test/makefile --checker tests_02
#
#
import argparse
import imp
import logging
import os
import shutil, sys
import tempfile

import utils
from app_runner import TestApplication

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("tmpdir", nargs="?", metavar="DIR", help="set working directory")
    parser.add_argument("--file", dest="test_file", metavar="FILE", help="set FILE for testing")
    parser.add_argument("--checker", dest="checker", metavar="NAME", help="set checker NAME")
    parser.add_argument("--gen-answers", dest="ansgen_mode", action="store_true", help="answers generation mode")
    parser.add_argument("--set-timeout", dest="time_limit", metavar="N", type=int, help="manually set timelimit in seconds")
    return parser.parse_args()


class Context:

    def __init__(self, tmpdir):
        if tmpdir is None:
            self.WorkingDirectory = tempfile.mkdtemp(dir=".")
            self.DirDeleteFlag = True
        else:
            if os.path.isdir(tmpdir):
                shutil.rmtree(tmpdir)
            os.makedirs(tmpdir)
            self.WorkingDirectory = tmpdir
            self.DirDeleteFlag = False
        self.InitLoggingSystem()

    def __del__(self):
        if self.DirDeleteFlag:
            shutil.rmtree(self.WorkingDirectory)        

    def InitLoggingSystem(self):
        logging.basicConfig(level=logging.DEBUG, format="\x1b[1m<%(levelname)s:%(module)s:%(funcName)s %(asctime)s>\x1b[m %(message)s")

    def CreateChecker(self, checker_name, timelimit=None):
        checker = self.CreateCheckerFromDir(checker_name) if os.path.isdir(checker_name) \
            else self.CreateCheckerFromFile("{0}.py".format(checker_name))
        if timelimit:
            checker.SetTimeLimit(timelimit)
        return checker
    
    def CreateCheckerFromFile(self, checker_file):
        mod = imp.load("checker", open(checker_file, "rt"), checker_file, (".py", "rt", imp.PY_SOURCE))
        return mod.Checker()

    def CreateCheckerFromDir(self, checker_dir):
        checker_file = os.path.join(checker_dir, "__init__.py")
        if not os.path.isfile(checker_file):
            raise RuntimeError("can't find checker in '{0}' directory".format(checker_dir))
        mod = imp.load_module("checker", open(checker_file, "rt"), checker_file, (".py", "rt", imp.PY_SOURCE))
        return mod.Checker(checker_dir)


class TestRunner:
    def __init__(self, args): 
        self.Run = self.GenerateCanonicAnswers if args.ansgen_mode \
                else self.RunTests
        self.TestContext = Context(args.tmpdir)
        self.TestingApp = TestApplication.Create(args.test_file, self.TestContext)
        self.Checker = self.TestContext.CreateChecker(args.checker, timelimit=args.time_limit)

    def DoTest(self, testIdx):
        self.Checker.CreateTestInput(os.path.join(self.TestContext.WorkingDirectory, "input.txt"), testIdx)
        exitCode, workingTime = self.TestingApp.Run(self.TestContext.WorkingDirectory, self.Checker.GetTimeLimit())
        if exitCode != 0:
            return False, "non-zero exit code {0}".format(exitCode)
        self.Print("[{0:.3f} secs] ".format(workingTime))
        return self.Checker.Check(os.path.join(self.TestContext.WorkingDirectory, "output.txt"), testIdx)

    def DoGenerate(self, testIdx):
        self.Checker.CreateTestInput(os.path.join(self.TestContext.WorkingDirectory, "input.txt"), testIdx)
        exitCode, workingTime = self.TestingApp.Run(self.TestContext.WorkingDirectory, self.Checker.GetTimeLimit())
        if exitCode != 0:
            raise RuntimeError("non-zero exit code {0} during answers generation".format(exitCode))
        self.Checker.TakeAnswer(os.path.join(self.TestContext.WorkingDirectory, "output.txt"), testIdx)
        return True, "success"

    def Print(self, message):
        sys.stdout.write(message)
        sys.stdout.flush()

    def PrintOk(self, testIdx):
        print "SUCCESS"

    def PrintWrong(self, testIdx, message):
        print "WA\n{1}".format(testIdx, message)

    def RunTests(self):
        for testIdx in xrange(1, self.Checker.GetTestsCount() + 1):
            self.Print("test #{0}: ".format(testIdx))
            result, message = self.DoTest(testIdx)
            if result:
                self.PrintOk(testIdx)
            else:
                self.PrintWrong(testIdx, message)
                return 1
        return 0

    def GenerateCanonicAnswers(self):
        for testIdx in xrange(1, self.Checker.GetTestsCount() + 1):
            self.Print("test #{0}: ".format(testIdx))
            result, message = self.DoGenerate(testIdx)
            self.PrintOk(testIdx)
        return 0


if __name__ == "__main__":
    try:
        exitCode = TestRunner(parse_args()).Run()
    except (utils.CompileError, utils.TimeLimitError) as e:
        print >>sys.stderr, e
        exitCode = 1
    sys.exit(exitCode)
