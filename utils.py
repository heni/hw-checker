import contextlib
import itertools
import logging
import operator, os
import re
import shutil, subprocess

def run_app(shell_args, **kws):
    app = subprocess.Popen(shell_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, **kws)
    out_data, err_data = app.communicate()
    return app.poll(), out_data, err_data


class CompileError(Exception):
    def __init__(self, exit_code, out_data, err_data):
        self.ExitCode = exit_code
        self.OutData = out_data
        self.ErrData = err_data
    def __str__(self):
        return "compilation exit code: {0.ExitCode}\noutput: {0.OutData}\nerrors: {0.ErrData}".format(self)

class TimeLimitError(Exception):
    def __init__(self, processed_time, limit):
        self.ProcessedTime = processed_time
        self.LimitTm = limit
    def __str__(self):
        return "time limit exceed: application were runned for {0.ProcessedTime:.3f} secs, limit is {0.LimitTm:.3f} secs".format(self)


class IChecker:
    def GetTestsCount(self): return self.TestsCount
    def GetTimeLimit(self): return self.TimeLimit
    def SetTimeLimit(self, time_limit): self.TimeLimit = time_limit
    def CreateTestInput(self, fpath, index): pass
    def Check(self, fpath, index): pass
    def TakeAnswer(self, fpath, index): pass


class FileChecker(IChecker):
    TimeLimit = 2

    def __init__(self, directory):
        self.tests = {}
        self.Directory = directory
        for fname in os.listdir(directory):
            sRes = re.match("(\d+)\.(in|out)$", fname)
            if sRes:
                index = int(sRes.group(1))
                fType = sRes.group(2)
                if index > 0:
                    self.tests.setdefault(index, {})[fType] = os.path.join(directory, fname)
                else:
                    logging.warning("testfile \"%s\" has bad index, will be ignored", fname)
        self.TestsCount = len(self.tests)

    def CreateTestInput(self, fpath, index):
        if index not in self.tests:
            raise RuntimeError("unknown test index %d" % index)
        shutil.copyfile(self.tests[index]["in"], fpath)

    def CompareLines(self, input_index, user_items, canonic_items):
        return all(itertools.starmap(operator.eq, itertools.izip_longest(user_items, canonic_items, fillvalue=None)))
           
    def Check(self, fpath, index):
        with contextlib.nested(open(fpath), open(self.tests[index]["out"])) as (user_out, canonic_out):
            user_items = (ln.strip() for ln in user_out)
            canonic_items = (ln.strip() for ln in canonic_out)
            if self.CompareLines(index, user_items, canonic_items):
                return True, "SUCCESS"
            return False, "incorrect answer"

    def TakeAnswer(self, fpath, index):
        ansFile = self.tests[index]["out"] = os.path.join(self.Directory, "{0:02d}.out".format(index))
        shutil.copyfile(fpath, ansFile)


class FloatComparator:
    def __init__(self, precision):
        self.EPS = 0.5 * 10**(-precision)
    def __call__(self, input_index, user_items, canonic_items):
        userItems = filter(bool, itertools.chain(*(ln.split() for ln in user_items)))
        canonicItems = filter(bool, itertools.chain(*(ln.split() for ln in canonic_items)))
        for uIt, cIt in itertools.izip_longest(userItems, canonicItems, fillvalue=None):
            if uIt is None or cIt is None:
                return False
            if uIt != cIt:
                try:
                    uf = float(uIt)
                    cf = float(cIt)
                except ValueError:
                    return False
                if abs(uf - cf) > self.EPS:
                    return False
        return True
