// sample solution
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>
using namespace std;

class TGraph {
public:
    typedef vector<int> adjacents_t;
    class TAdjacentsIterator {
    public:
        typedef adjacents_t::const_iterator TInternalIterator;
    private:
        TInternalIterator Current;
        TInternalIterator End;
    public:
        TAdjacentsIterator(TInternalIterator begin, TInternalIterator end) 
            : Current(begin)
            , End(end) {}

        const int& operator*() const{
            return *Current;
        }   

        bool IsValid() const {
            return Current != End;
        }

        TAdjacentsIterator& operator++() {
            if (IsValid()) 
                ++Current;
            return *this;
        }
    };

private:
    map<int, adjacents_t> Nodes;

public:
    TGraph() {}
    void AddEdge(int a, int b) {
        map<int, adjacents_t>::iterator eIt = Nodes.find(a);
        if (eIt == Nodes.end()) {
            eIt = Nodes.insert(pair<int, adjacents_t>(a, adjacents_t())).first;
        }
        eIt->second.push_back(b);
    }

    TAdjacentsIterator GetAdjacents(int a) const {
        map<int, adjacents_t>::const_iterator it = Nodes.find(a);
        if (it == Nodes.end()) {
            static adjacents_t empyNodes;
            return TAdjacentsIterator(empyNodes.begin(), empyNodes.end());
        }
        return TAdjacentsIterator(it->second.begin(), it->second.end());
    }
};

class TGraphWalker {
private:
    const TGraph& Graph;

    void RunDFS(int a, map<int, int>& parents) const {
        vector<int> vstNodes;
        vstNodes.push_back(a);
        while (! vstNodes.empty()) {
            int v = vstNodes.back();
            vstNodes.pop_back();
            for (TGraph::TAdjacentsIterator it = Graph.GetAdjacents(v); it.IsValid(); ++it) {
                int u = *it;
                if (u != a && parents.count(u) == 0) {
                    parents[u] = v;
                    vstNodes.push_back(u);
                }
            }
        }
    }
public:
    TGraphWalker(const TGraph& g) 
        : Graph(g)
    {}

    bool SearchPath(int a, int b, vector<int>& path) const {
        map<int, int> parents;
        if (a != b) {
            RunDFS(a, parents);
            if (parents.count(b) == 0) 
                return false;
        }
        path.clear();
        while (true) {
            path.push_back(b);
            if (parents.count(b) == 0)
                break;
            b = parents[b];
        }
        reverse(path.begin(), path.end());
        return true;
    }
};


int main() {
    ifstream in("input.txt");
    ofstream out("output.txt");

    TGraph g;
    TGraphWalker walker(g);
    int n, m;
    in >> n >> m;
    for (int i = 0; i < m; ++i) {
        int a, b;
        in >> a >> b;
        g.AddEdge(a, b);
    }
    int testCnt;
    in >> testCnt;
    for (int i = 0; i < testCnt; ++i) {
        int a, b;
        vector<int> path;
        in >> a >> b;
        if (walker.SearchPath(a, b, path)) {
            for (int j = 0; j < path.size(); ++j) 
                out << path[j] << " ";
        } 
        out << endl;
    }
    return 0;
}
