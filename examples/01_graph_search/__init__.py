import itertools
import utils
class Checker(utils.FileChecker):
    def Check(self, fpath, index):
        canonic_in = self.tests[index]["in"]
        canonic_out = self.tests[index]["out"]
        return SolutionChecker(canonic_in).Check(fpath, canonic_out)


class SolutionChecker(object):
    def __init__(self, inputFile):
        reader = open(inputFile)
        self.Graph = self.ReadGraph(reader)
        self.Pairs = self.ReadPairs(reader)

    @classmethod
    def ReadGraph(cls, reader):
        graph, components = {}, {}
        n, m = map(int, reader.next().split())
        for _ in xrange(m):
            a, b = map(int, reader.next().split())
            graph.setdefault(a, set()).add(b)
        return graph

    @classmethod
    def ReadPairs(cls, reader):
        tCnt = int(reader.next())
        pairs = [map(int, ln.split()) for ln in itertools.islice(reader, tCnt)]
        assert len(pairs) == tCnt
        return pairs

    def Check(self, testFile, cnncFile):
        for pair, tLn, cLn in itertools.izip_longest(self.Pairs, map(str.strip, open(testFile)), map(str.strip,open(cnncFile)), fillvalue=None):
            if pair is None and tLn:
                return False, "too many lines in output"
            if pair:
                a, b = pair
                if (not tLn) ^ (not cLn):
                    return False, "incorrect answer"
                elif tLn:
                    tPath = map(int, tLn.split())
                    if not(tPath[0] == a and tPath[-1] == b):
                        return False, "incorrect start/finish vertices"
                    for u, v in zip(tPath, tPath[1:]):
                        if v not in self.Graph.get(u, set()):
                            return False, "incorrect path"
        return True, "success"
