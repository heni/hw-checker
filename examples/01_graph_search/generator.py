#!/usr/bin/env python2.7
import random
import itertools
MAXV = 100
MAXT = 100

class Test(object):

    def __init__(self, edges, pairs):
        self.Edges = edges
        self.Pairs = pairs

    def Print(self, fpath):
        with open(fpath, "w") as test_printer:
            vSet = set(itertools.chain(*self.Edges))
            print >>test_printer, len(vSet), len(self.Edges)
            for u, v in self.Edges:
                print >>test_printer, u, v
            print >>test_printer, len(self.Pairs)
            for u, v in self.Pairs:
                if u not in vSet or v not in vSet:
                    raise RuntimeError("incorrect test request")
                print >>test_printer, u, v


def random_value_from_range(xval, xrng, xmax):
    return xval if xval is not None \
        else random.randint(*xrng) if xrng is not None \
        else random.randint(1, xmax)
        

class RandomGraph:
    
    def __genEdges(self, edges_to_gen):
        max_edges = len(self.vList) * (len(self.vList) - 1)
        self.edges = []
        for u in self.vList:
            for v in self.vList:
                if u != v:
                    if random.randint(1, max_edges) <= edges_to_gen:
                        self.edges.append((u, v))
                        max_edges -= 1
                        edges_to_gen -= 1
        random.shuffle(self.edges)

    def __init__(self, vcnt=None, vrange=None, ecnt=None, erange=None):
        vCnt = random_value_from_range(vcnt, vrange, MAXV)
        eCnt = random_value_from_range(ecnt, erange, vCnt**2)
        self.vList = range(vCnt)
        self.__genEdges(eCnt)
        self.vList = sorted(set(itertools.chain(*self.edges)))


class TestWithRandomGraph(Test):
    def __init__(self, vcnt=None, vrange=None, ecnt=None, erange=None, tcnt=None, trange=None):
        graph = RandomGraph(vcnt, vrange, ecnt, erange)
        tCnt = random_value_from_range(None, None, min(len(graph.edges), MAXT))
        tests = [(random.choice(graph.vList), random.choice(graph.vList)) for _ in xrange(tCnt)]
        super(TestWithRandomGraph, self).__init__(graph.edges, tests)

       
class TestSuite(object):
    def __init__(self):
        self.tests = [
            TestWithRandomGraph(vcnt=5, ecnt=10, tcnt=5),
            TestWithRandomGraph(vrange=(1, 20), erange=(1, 100)),
            TestWithRandomGraph(vrange=(1, 50), erange=(1, 400)),
            TestWithRandomGraph(vrange=(1, 100), erange=(1, 100)),
            TestWithRandomGraph(vrange=(1, 100), erange=(1, 200)),
            TestWithRandomGraph(vrange=(1, 100), erange=(1, 300)),
            TestWithRandomGraph(vrange=(1, 100), erange=(1, 500)),
            TestWithRandomGraph(vrange=(1, 100), erange=(1, 1000)),
            TestWithRandomGraph(vrange=(1, 100), erange=(1, 5000)),
            TestWithRandomGraph(vcnt=100, ecnt=10000, tcnt=100)
        ]
    def Print(self, start_index):
        for index, test in zip(itertools.count(start_index), self.tests):
            fpath = "{0:02d}.in".format(index)
            test.Print(fpath)

if __name__ == "__main__":
    TestSuite().Print(4)
